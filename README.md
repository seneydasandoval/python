**CI/CD con Django y HEROKU**

![Image of Yaktocat](screenshots/Sketch3.png)

Git tiene herramientas permiten manejar nuestro código, compilarlo y realizar diferentes acciones y pruebas al resultado. 
Si estas acciones se realizan de forma ágil podríamos decir que estamos usando integración continua, a continuación se 
define el proceso de integración continua realizado:
La aplicación corresponde a un login simple, está hecho en python y usa el framework Django, la base de datos postgres, y 
tiene dos pruebas unitarias para configuración del proceso de despliegue o ‘pipeline’ se empieza creando un fichero llamado 
‘.gitlab-ci.yml’ en la raíz del proyecto que está configurado de la siguiente manera: 
image: se corresponde a la imagen de Docker que va a contener todo el proceso de creación y empaquetado del contenedor, en este caso python.
stages: nos permiten organizar los diferentes jobs que hagamos. Los que tengan el mismo stage se ejecutan a simultáneamente y los qué no en diferentes, en el archivo hay 4 stages :
* [ ] ver: print la version de python 
* [ ] init: se inicializa e instalan las dependencias (ver y init) corresponden a la etapa de **desarrollo**
* [ ] tests: en test se instalan las dependencias y se corren los test qué corresponde a la etapa de **homologacion**
* [ ] deploy: la aplicación se deploya en dos ambientes tanto desarrollo y producción, el deploy en producción se realiza de forma manual si pasa todas las etapas correctamente.
* [ ] services: GitLab CI usa la services para definir qué contenedores usar en esté caso de uso el de postgresql (https://docs.gitlab.com/ee/ci/services/postgres.html)
* [ ] when: indica cuando deployar la siguiente etapa
* [ ] only master las indicaciones solo se ejecutan sobre la rama master la etapa deploy pertenece al ambiente de **producción.**
* [ ] https://testgccprod.herokuapp.com/admin/login/?next=/
* [ ] https://testgccprod.herokuapp.com/admin/login/?next=/






![Image of Yaktocat](screenshots/Sketch2.png)