# GCC TP2 - 2019 
# Prueba unitaria para el inicio de sesion.
from django.test import TestCase
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType

class usuarioTestCase(TestCase):
    def setUp(self):



        a1 = User.objects.create_user(username='test1',
                                      password='mypassword',
                                      first_name='test1',
                                      last_name='test1',
                                      email='test1@testmail.com')
        a2 = User.objects.create_user(username='test2',
                                      password='mypassword',
                                      first_name='test2',
                                      last_name='test2',
                                      email='test2@testmail.com')

    def test_crearUsuario1(self):
        user1 = User.objects.get(username='test1')
        self.assertEqual(user1.first_name, "test1", "Prueba Unitaria Usuario: El nombre de usuario coincide")

    def test_crearUsuario41(self):
        user1 = User.objects.get(username='test1')
        self.assertEqual(user1.last_name, "test1", "Prueba Unitaria Usuario: El apellido del usuario coincide")

